package helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.minidev.json.JSONObject;

public class DBhandler {

    private static String connectionURL = "jdbc:sqlserver://localhost:123;database=ABC;user=sa;password=pwd";

    public static void addNewJobWithName(String jobName){
        try(Connection connect = DriverManager.getConnection(connectionURL)){
            connect.createStatement().execute("sql('"+jobName+"')");

        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static JSONObject  getMinAndMaxLevelsForJob(String jobName){
        JSONObject json = new JSONObject();

        try(Connection connect = DriverManager.getConnection(connectionURL)){
            ResultSet rs = connect.createStatement().executeQuery("sql('"+jobName+"')");
            rs.next();
            json.put("val1", rs.getString("val1"));

        } catch (SQLException e){
            e.printStackTrace();
        }

        return json;
    }
    
}
