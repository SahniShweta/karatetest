
Feature: Sign Up new user

Background: Preconditions
    * def dataGenerator = Java.type('helpers.DataGenerator')
    * def randomEmail = dataGenerator.getRandomEmail()
    * def randomUsername = dataGenerator.getRandomUsername()
    Given url apiUrl

Scenario: New User Sign Up
    # Given def userData = {"email": "KarateUser893@test.com","username": "KarateUser832"}
    * print randomEmail
    * def jsFunction = 
    """
        function(){
            var DataGenerator = Java.type('helpers.DataGenerator')
            var generator = new DataGenerator()
            return generator.getRandomUsername2()
        }
    """
    
    * def randomUsername2 = call jsFunction
    Given path 'users'
    And request
    """
        {
            "user": {
                "email": #(randomEmail),
                "password": "KarateUser123",
                "username": #(randomUsername)
            }
        }
    """
    When method Post
    Then status 200
    And match response ==
    """
        {
            "user": {
                "email": #(randomEmail),
                "username": #(randomUsername),
                "bio": null,
                "image": null,
                "token": "#string"
            }
        }
    """

    Scenario Outline: Validate Sign Up error messages
        # Given def userData = {"email": "KarateUser893@test.com","username": "KarateUser832"}    
        
        Given path 'users'
        And request
        """
            {
                "user": {
                    "email": "<email>",
                    "password": "<password>",
                    "username": "<username>"
                }
            }
        """
        When method Post
        Then status 422
        And match response == <errorResponse>

        Examples:
            | email                | password      | username               | errorResponse                                         |
            | #(randomEmail)       | KarateUser123 | KarateUser1            | {"errors":{"username":["has already been taken"]}}    |
            | KarateUser1@test.com | KarateUser123 | #(randomUsername)      | {"errors":{"email":["has already been taken"]}}       |
            | KarateUser1          | KarateUser123 | #(randomUsername)      | {"errors":{"email":["has already been taken"]}}       |
            | #(randomEmail)       | KarateUser123 | KarateUser123123123123 | {"errors":{"username":["has already been taken"]}}    |
            | #(randomEmail)       | Kar           | #(randomUsername)      | {"errors":{"password":["has already been taken"]}}    |
            |                      | KarateUser123 | #(randomUsername)      | {"errors":{"email":["can't be blank"]}}               |
            | #(randomEmail)       |               | #(randomUsername)      | {"errors":{"password":["can't be blank"]}}            |
            | #(randomEmail)       | KarateUser123 |                        | {"errors":{"username":["can't be blank"]}}            |

    