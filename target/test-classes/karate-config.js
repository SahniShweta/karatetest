function fn() {
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev';
  }
  var config = {
    apiUrl: 'https://conduit.productionready.io/api/'
  }
  if (env == 'dev') {
    config.userEmail = 'karate888@test.com'
    config.userPassword = 'Karate123'
    config.userName='Karate888'
  } 
  if (env == 'qa') {
    config.userEmail = 'karate2@test.com'
    config.userPassword = 'Karate456'
  }

var accessToken = karate.callSingle('classpath:helpers/CreateToken.feature', config).authtoken;
karate.configure('headers', {Authorization: 'Token ' + accessToken});

  return config;
}