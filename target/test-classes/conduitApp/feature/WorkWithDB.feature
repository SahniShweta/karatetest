

Feature: Work with DB
    
    Background: connection DB
        * def dbHandler = Java.type('helpers.DBhandler')

    
    Scenario: Seed databse with a new job
        * eval dbHandler.addNewJobWithName("QA")

    
    Scenario: Get level
        * def level =  dbHandler.getMinAndMaxLevelsForJob("QA")
        * print level.val1
        And match level.val1 == 'abc'