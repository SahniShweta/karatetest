
Feature: Home Work

    Background: Preconditions
        * url apiUrl 
        * def isValidTime = read('classpath:helpers/timeValidator.js')
         # Step 1: Get articles of the global feed
         Given params {limit:10, offset:0}
         Given path 'articles'
         When method Get
         Then status 200
         # Step 2: Get the favorites count and slug ID for the first arice, save it to variables
         * def slugID = response.articles[0].slug
         * def favCount =  response.articles[0].favoritesCount
        
         * print "=======================  "+favCount
         * print "=======================  "+slugID
  
    Scenario: Favorite articles
       
        # Step 3: Make POST request to increse favorites count for the first article

        Given path 'articles/' + slugID + '/favorite'
        And request {}
        When method Post
        Then status 200
        # Step 4: Verify response schema
        And match response ==
        """
            {
                "article": {
                    "slug": "#string",
                    "title": "#string",
                    "description": "#string",
                    "body": "#string",
                    "tagList": "#array",
                    "createdAt": "#? isValidTime(_)",
                    "updatedAt": "#? isValidTime(_)",
                    "favorited": '#boolean',
                    "favoritesCount": '#number',
                    "author": {
                        "username": "#string",
                        "bio": "##string",
                        "image": "#string",
                        "following": '#boolean'
                    }
                }
            }
        """
        # Step 5: Verify that favorites article incremented by 1
            #Example
        * print "***************** = " +response.article.favoritesCount
        And match response.article.favoritesCount == favCount + 1
        
        # Step 6: Get all favorite articles
        Given params {limit:10, offset:0, favorited: "#(userName)"}
        Given path 'articles'
        When method Get
        Then status 200
        # Step 7: Verify response schema
        And match response == {"articles":"#array", "articlesCount":"#number"}
        And match each response.articles ==
        """
                 {
                        "slug": "#string",
                        "title": "#string",
                        "description": "#string",
                        "body": "#string",
                        "tagList": "#array",
                        "createdAt": "#? isValidTime(_)",
                        "updatedAt": "#? isValidTime(_)",
                        "favorited": '#boolean',
                        "favoritesCount": '#number',
                        "author": {
                            "username": "#string",
                            "bio": "##string",
                            "image": "#string",
                            "following": '#boolean'
                        }
                    }
        """
        # Step 8: Verify that slug ID from Step 2 exist in one of the favorite articles
        And match response.articles[*].slug contains slugID

        Given path 'articles/' + slugID + '/favorite'
        And request {}
        When method Delete
        Then status 200

    
    Scenario: Comment articles
        # Step 3: Make a GET call to 'comments' end-point to get all comments
        Given path 'articles/'+slugID+'/comments'
        When method Get
        Then status 200
        # Step 4: Verify response schema
       And match each response.comments ==
       """
        {
            "id": '#number',
            "createdAt": "#? isValidTime(_)",
            "updatedAt": "#? isValidTime(_)",
            "body": "#string",
            "author": {
                 "username": "#string",
                 "bio": '##string',
                 "image": "#string",
                 "following": '#boolean'
             }
         }
       """
    #     # Step 5: Get the count of the comments array lentgh and save to variable
    #         #Example
    #        # * def responseWithComments = [{"article": "first"}, {article: "second"}]
             * def articlesCount = response.comments.length
             *  print "***************articlesCount=" +articlesCount
    #     # Step 6: Make a POST request to publish a new comment
            Given path 'articles/'+slugID+'/comments'
            And request {"comment":{"body":"Test1"}}
            When method Post
            Then status 200
            * def commentid = response.comment.id
    #     # Step 7: Verify response schema that should contain posted comment text
        And match response.comment ==
        """
             {
                    "id": '#number',
                    "createdAt": "#? isValidTime(_)",
                    "updatedAt": "#? isValidTime(_)",
                    "body": "#string",
                    "author": {
                        "username": "#string",
                        "bio": '##string',
                        "image": "#string",
                        "following": '#boolean'
                    }
                }
            
        """
    #     # Step 8: Get the list of all comments for this article one more time
            Given path 'articles/'+slugID+'/comments'
            When method Get
            Then status 200
            * def commentCount = response.comments.length
            * print "*************** commentCount= " +commentCount
        
    #     # Step 9: Verify number of comments increased by 1 (similar like we did with favorite counts)
             And match commentCount == articlesCount + 1
    #     # Step 10: Make a DELETE request to delete comment
            Given path 'articles/'+slugID+'/comments/'+commentid
            When method Delete
            Then status 200
    #     # Step 11: Get all comments again and verify number of comments decreased by 1
            Given path 'articles/'+slugID+'/comments'
            When method Get
            Then status 200
             * def commentsCount = response.comments.length
             * print "*********newCount=" +commentsCount
            And match commentsCount  == articlesCount
   